## BANG! Character Randomizer: Java Edition

v1.1

by Sean Castillo


## Character Input Instructions

-The BANG! characters must be written in Characters.txt, one per line.
-Make sure there are no blank lines, as they would be counted as characters.
-The characters should be written in the correct order (at least by volume)
 so that the full roster print may be accurate.


## Running Instructions

(Note: The commands are case-sensitive.)

0.Have Java installed.

1.On Command Prompt, change the directory to the location of the .class files
  (For example, if the folder is just on the Desktop, type

	cd Desktop/BANG!_CR

  )


2.Type 'java Driver' sans the quotation marks.

