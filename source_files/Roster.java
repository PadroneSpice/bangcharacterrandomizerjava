import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.Scanner;

/**
*
* @author Sean Castillo
* @version v1.0
* @date 27 Dec 2015
*/

public class Roster
{
	
	private ArrayList<Character> roster;
	int rosterCount;
	
	public Roster()
	{
		roster = new ArrayList<>();
		rosterCount = 0;
	}
	
	public void buildRoster(Scanner tf)
	{
		int characterNumber = 0;
		
		while (tf.hasNextLine())
		{
			String characterName = tf.nextLine();
			characterNumber++;
			
			Character c = new Character(characterName, characterNumber);
			roster.add(c);
			rosterCount++;
		}

		setRosterCount(rosterCount);
	}
	
	public void printRoster()
	{
		int volumeCounter = 1;
		int volumeCharacterCounter = 1;
		
		for (Character c : roster)
		{  
			if (volumeCharacterCounter == 1)
			{
				System.out.println("___Volume " + volumeCounter + "___");
				volumeCharacterCounter++;
			}
			System.out.println(c.toString());
			
			if ((c.getNumber() % 8) == 0) //Adds spaces between groups of 8 characters, which make up the volumes.
			{
				System.out.println();
				volumeCounter++;
				volumeCharacterCounter = 1;
			}
		}
	}
	
	public void shuffleRoster()
	{
		long seed = System.nanoTime();
		Collections.shuffle(roster, new Random(seed));
		Collections.shuffle(roster, new Random(seed));
	}
	
	public void setRosterCount(int cc)
	{
		rosterCount = cc;
	}
	
	public int getRosterCount()
	{
		return rosterCount;
	}
	
	public ArrayList<Character> getRoster()
	{
		return roster;
	}
}
