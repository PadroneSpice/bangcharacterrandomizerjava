import java.util.Scanner;
import java.io.IOException;
import java.io.File;
/**
*
* @author Sean Castillo
* @version v1.0
* @date 27 Dec 2015
*/

public class Driver
{	
	public static void main (String[] args) throws IOException
	{
		Scanner kb = new Scanner(System.in);
		int playerCount = 0;
		int choiceCount = 0;
		
		//prompts for input
		System.out.println("Enter the number of players: ");
		playerCount = kb.nextInt();
		System.out.println("Enter the number of characters from which each player may choose: ");
		choiceCount = kb.nextInt();
		
		//text file input
		Scanner tf = new Scanner(new File("Characters.txt"));
		
		//checks input
		InputCheck c = new InputCheck(playerCount, choiceCount, tf);
	
		
		if (c.inputValid() == true)
		{	
			//building the roster
			Scanner tf2 = new Scanner(new File("Characters.txt")); //because Scanner is stupid about hasNextLine. 
			Roster characterRoster = new Roster();
			characterRoster.buildRoster(tf2);
			
			//printing
			Print p = new Print(playerCount, choiceCount, characterRoster);
			
			p.print();
		}
		else
		{
			System.out.println("Input is invalid yo.");
		}
		
		
		tf.close();
	}
}
