import java.util.Scanner;

/**
*
* @author Sean Castillo
* @version v1.1
* @date 27 Dec 2015
*/

public class InputCheck
{
	private int playerCount;
	private int choiceCount;
	private int characterCount;
	
	public InputCheck(int pc, int cc, Scanner tf)
	{
		playerCount = pc;
		choiceCount = cc;
		
		characterCount = 0;
		while (tf.hasNextLine())
		{
			characterCount++;
			tf.nextLine();
		}
	}
	
	public boolean inputValid()
	{
		boolean isValid = false;
				
		if (playerCount < 1)
		{
			System.out.println("Error 1: Invalid number of players.");
		}
		else if (choiceCount < 1)
		{
			System.out.println("Error 2: Invalid number of choices per player.");
		}
		else if (characterCount < 1)
		{
			System.out.println("Error 3: Invalid number of characters in input file.");
		}
		else if (playerCount > characterCount)
		{
			System.out.println("Error 4: Not enough characters for that number of players");
		}
		else if ((characterCount / choiceCount) < playerCount)
		{
			System.out.println("Error 5: Not enough characters for that number of choices and players");
		}
		else
		{
			isValid = true;
		}
		
			
		return isValid;
	}
}
