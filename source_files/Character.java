/**
 *
 * @author Sean Castillo
 * @version v1.0
 * @date 27 Dec 2015
 */

public class Character
{
	private String name;
	private int number;
	
	public Character(String characterName, int characterNumber)
	{
		name = characterName;
		number = characterNumber;
	}
	
	public String toString()
	{
		return (number + ": " + name);
	}
	
	public String getName()
	{
		return name;
	}
	
	public int getNumber()
	{
		return number;
	}
}
