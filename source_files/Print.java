/**
 *
 * @author Sean Castillo
 * @version v1.0
 * @date 27 Dec 2015
 */


public class Print
{
	private int playerCount;
	private int choiceCount;
	private Roster characterRoster;
	
	public Print(int pc, int cc, Roster cr)
	{
		playerCount = pc;
		choiceCount = cc;
		characterRoster = cr;
	}
	
	
	public void print()
	{
		characterRoster.printRoster(); //Prints the roster
		characterRoster.shuffleRoster(); //Randomizes the roster
		
		System.out.println("The number of players is: " + playerCount);
		System.out.println("The number of characters from which each player may choose is: " + choiceCount);
		
		System.out.println();
		
		int cdc = 0; //character distributed count
		
		for (int i = 1; i < playerCount + 1; i++)  //structured like this to start on player 1 instead of player 0.
		{
			System.out.println("Player " + i + ":");
			
			for (int j = 0; j < choiceCount; j++)
			{
				//prints the character here
				System.out.println(characterRoster.getRoster().get(cdc).toString());
				cdc++;
			}
			
			System.out.println();
		}
	}
}
